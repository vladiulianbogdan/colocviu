package ro.pub.cs.systems.pdsd.practicaltest02;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

public class PracticalTest02MainActivity extends Activity implements OnClickListener {

	EditText e;
	Button launchB;
	WebView w;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_practical_test02_main);
		
		e = (EditText) findViewById(R.id.editText1);
		launchB = (Button) findViewById(R.id.button1);
		w = (WebView) findViewById(R.id.webView1);
		
		launchB.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		ClientThread client = new ClientThread();
		ServerThread server = new ServerThread();
		
		client.url = "http://ocw.cs.pub.ro/courses/\n";
		client.url = e.getText().toString() + "\n";
		client.w = w;
		
		server.start();
		client.start();
	}

}

class ClientThread extends Thread
{
	Socket s;
	String url;
	WebView w;
	
	@Override
	public void run() {
		super.run();
		
		try {
			s = new Socket("localhost", 3334);
			BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(s.getOutputStream());
			PrintWriter printWriter = new PrintWriter(bufferedOutputStream, true);
			printWriter.write(url);
			printWriter.flush();
			
			Log.d("MYTAG", "Clientul a scris" + url);
			
			InputStreamReader inputStreamReader = new InputStreamReader(s.getInputStream());
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			
			String temp = bufferedReader.readLine();
			String content  = "";
			while(temp != null)
			{
				content += temp;
				temp = bufferedReader.readLine();
			}
			
			final String finalContent = content;
			
			w.post(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					w.loadData("<html>" + finalContent + "</html>", "text/html", "utf-8");
				}
			});
			Log.d("MYTAG", "RECEIVE LA CLIENT! " + content);
			bufferedReader.close();
			printWriter.close();
			s.close();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


class ServerThread extends Thread
{
	ServerSocket s;
	boolean running = true;
	
	@Override
	public void run() {
		super.run();
		
		try {
			s = new ServerSocket(3334);
			while(running)
			{
				Socket q = s.accept();
				Log.d("MYTAGE", "Accept connection!");
				InputStreamReader inputStreamReader = new InputStreamReader(q.getInputStream());
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				
				BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(q.getOutputStream());
				PrintWriter printWriter = new PrintWriter(bufferedOutputStream, true);
				
				String url = bufferedReader.readLine();
				
				Log.d("MYTAG", "Pe server primit " + url);
				
				if (!url.contains("bad"))
				{
					Log.d("MYTAG", "Face requestul la web ");
					try {
						  HttpClient httpClient = new DefaultHttpClient();
						  HttpGet httpGet = new HttpGet(url);
						  HttpResponse httpGetResponse = httpClient.execute(httpGet);
						  HttpEntity httpGetEntity = httpGetResponse.getEntity();
						  if (httpGetEntity != null) {  
						    // do something with the response
							  String htmlContent = getContent(httpGetEntity);
						    
						    Document document = Jsoup.parse(htmlContent);
							Element body = document.body();
							String bodyContent = body.html();
							printWriter.write(bodyContent);
							printWriter.flush();
						  }            
						  else
						  {
							  Log.d("MYTAG", "E NULL!!");
						  }
						} catch (Exception exception) {
							Log.d("MYTAG", exception.getMessage());
						  }
				}
				else
				if (url.contains("bad"))
				{
					printWriter.write("EROARE!");
					printWriter.flush();
				}
				
				bufferedReader.close();
				printWriter.close();
				q.close();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	String getContent(HttpEntity entity)
	{
		BufferedReader bufferedReader;
		StringBuilder result = new StringBuilder();
		try {
		  // ...
		  bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent()));
		  int currentLineNumber = 0;
		  String currentLineContent;
		  while ((currentLineContent = bufferedReader.readLine()) != null) {
		    currentLineNumber++;
		    result.append(currentLineNumber + ": " + currentLineContent + "\n");
		  }
		  
		  return result.toString();
		  
		} catch (Exception exception) {
		  Log.e("MYTAG", exception.getMessage());
		  
		}
		
		return null;
	}
}